#ifndef SHUTDOWN_H_
#define SHUTDOWN_H_

typedef enum
{
    SD_PO_GATE_ALL_SHORT,
    SD_PO_GATE_H_SHORT,
    SD_PO_GATE_L_SHORT,
    SD_PO_GATE_H_OR_L_OPEN,

    SD_RU_GATE_H_SHORT,
    SD_RU_GATE_H_OR_L_OPEN,
    
    SD_FET_NTC_HOT,

}shutdown_cause_t;

void Shutdown_Activate(shutdown_cause_t const sd);

#endif//SHUTDOWN_H_

