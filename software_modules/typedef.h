#ifndef TYPEDEF_H_
#define TYPEDEF_H_

#define BIT(x)  (1<<x)

typedef enum
{
    OFF=0,
    ON,
    NUM_ON_OFF_STATE,
}onoff_state_t;


#endif//TYPEDEF_H_

