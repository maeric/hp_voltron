#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "software_modules/delay.h"

void Delay_Init(void)
{
    //Initialize RTC
    RTC_init(RTC_BASE,
        0xFFFF,
        RTC_CLOCKPREDIVIDER_1);
}

void Delay_Us(uint16_t time_us)
{
    //Start RTC Clock with clock source SMCLK
    RTC_start(RTC_BASE, RTC_CLOCKSOURCE_SMCLK);

    while (HWREG16(RTC_BASE + OFS_RTCCNT) < (time_us*8))
    {
        _nop();
    }
}

void Delay_Ms(uint16_t time_ms)
{
    uint16_t i;
    for(i=0;i<time_ms;i++)
    {
        Delay_Us(1000);
    }
}



