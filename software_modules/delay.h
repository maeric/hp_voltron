#ifndef HARDWARE_DELAY_H_
#define HARDWARE_DELAY_H_

void Delay_Init(void);
void Delay_Us(uint16_t time_us);
void Delay_Ms(uint16_t time_ms);

#endif//HARDWARE_DELAY_H_

