#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "config.h"
#include "software_modules/shutdown.h"
#include "software_modules/delay.h"
#include "hardware/gate_driver.h"
#include "hardware/uart.h"
#include "hardware/fault_output.h"

void Shutdown_Activate(shutdown_cause_t const sd)
{
    GD_Gate_Ctrl(GATE_H,OFF);
    GD_Gate_Ctrl(GATE_L,OFF);
    Fault_Out_Ctrl(FAULT_OCCURRED);


    switch (sd)
    {
        case SD_PO_GATE_ALL_SHORT:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_PO_A_SHORT\r\n");
            #endif
            break;

        case SD_PO_GATE_H_SHORT:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_PO_H_SHORT\r\n");
            #endif
            break;

        case SD_PO_GATE_L_SHORT:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_PO_L_SHORT\r\n");
            #endif
            break;

        case SD_PO_GATE_H_OR_L_OPEN:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_PO_FET_OPEN\r\n");
            #endif
            break;

        case SD_RU_GATE_H_SHORT:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_RU_H_SHORT\r\n");
            #endif
            break;

        case SD_RU_GATE_H_OR_L_OPEN:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_RU_H_OR_L_OPEN\r\n");
            #endif
            break;

        case SD_FET_NTC_HOT:
            #if CFG_UART_ENABLE == true
            UART_Send_String("SD_FET_HOT\r\n");
            #endif
            break;

        default:
            break;
    }

    while (true)
    {
        //Delay_Ms(300);
        //UART_Send_String("in SD\r\n");
    }
}


