#ifndef FAULT_OUTPUT_H_
#define FAULT_OUTPUT_H_

#include "software_modules/typedef.h"


#define FAULT_OUT_PORT_DIR  P2DIR
#define FAULT_OUT_PORT_OUT  P2OUT
#define FAULT_OUT_PIN       BIT(6)

typedef enum
{
    FAULT_NONE=0,
    FAULT_OCCURRED,
    NUM_FAULT_STATE,
}fault_state_t;

void Fault_Out_Init(void);
void Fault_Out_Ctrl(fault_state_t state);

#endif//FAULT_OUTPUT_H_

