#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "hardware/adc.h"


void ADC_Init(void)
{
    //port init
    //ADC_CH_VBUS
    //GPIO_setAsPeripheralModuleFunctionInputPin(ADC_VBUS_PORT,ADC_VBUS_PIN,GPIO_PRIMARY_MODULE_FUNCTION);

    // Configure ADC10
    ADCCTL0 |= ADCSHT_2 | ADCON;                             // ADCON, S&H=16 ADC clks
    ADCCTL1 |= ADCSHP;                                       // ADCCLK = MODOSC; sampling timer
    ADCCTL2 |= ADCRES_1;                                       // 10-bit conversion results
    //ADCMCTL0 |= ADCINCH_1;                                   // A1 ADC input select; Vref=AVCC
    //ADCIE |= ADCIE0;                                         // Enable ADC conv complete interrupt
}

uint16_t ADC_Read_Value(uint16_t adc_channel)
{
#if 0
    uint16_t i;

    volatile uint16_t ADC_Value=0;
    uint16_t ADC_Sum=0;
    uint16_t ADC_Max=0;
    uint16_t ADC_Min=0xFFFF;

    ADCMCTL0 |= adc_channel;                        //chose ADC channel
    for(i=8;i>0;i--)
    {
        ADCCTL0 |= ADCENC | ADCSC;                           // Sampling and conversion start
        //j = 0;
        while(((ADCCTL0 & ADCSC)==1));// && (j<1000))
        //{
            //_nop();
            //j++;
        //}
        ADC_Value = ADCMEM0;                     
        ADC_Sum += ADC_Value;

        if (ADC_Value > ADC_Max)
        {
            ADC_Max = ADC_Value;
        }
        else if(ADC_Value < ADC_Min)
        {
            ADC_Min =ADC_Value;
        }
        
    }
    ADCCTL0 &= ~ADCENC;                         //Disable ADC
    ADCMCTL0 &= ~adc_channel;                       //clear ADC channel

    ADC_Sum -= ADC_Max;
    ADC_Sum -= ADC_Min;

    ADC_Sum = ADC_Sum/6;
    return ADC_Sum;
#endif

    uint16_t i;

    uint16_t ADC_Value;
    ADC_Value = 0;

    ADCMCTL0 |= adc_channel;                        //chose ADC channel
    for(i=8;i>0;i--)
    {
        ADCCTL0 |= ADCENC | ADCSC;                           // Sampling and conversion start
        //j = 0;
        while(((ADCCTL0 & ADCSC)==1));// && (j<1000))
        //{
            //_nop();
            //j++;
        //}
        ADC_Value += ADCMEM0;                        //read and add ADC result
    }
    ADCCTL0 &= ~ADCENC;                         //Disable ADC
    ADCMCTL0 &= ~adc_channel;                       //clear ADC channel

    ADC_Value = ADC_Value>>3;
    return ADC_Value;
}
