#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "hardware/fault_output.h"

void Fault_Out_Init(void)
{
    FAULT_OUT_PORT_DIR &=~ FAULT_OUT_PIN;
}


void Fault_Out_Ctrl(fault_state_t state)
{
    if(state == FAULT_OCCURRED)
    {
        FAULT_OUT_PORT_DIR |= FAULT_OUT_PIN;
        FAULT_OUT_PORT_OUT &=~  FAULT_OUT_PIN;
    }
    else if(state == FAULT_NONE)
    {
        FAULT_OUT_PORT_DIR &=~ FAULT_OUT_PIN;
    }
}


