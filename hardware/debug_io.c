#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "config.h"
#include "hardware/debug_io.h"

#if CFG_DEBUG_IO_ENABLE == true
void Debug_IO_Init(void)
{
    GPIO_setAsOutputPin(GPIO_PORT_P1,GPIO_PIN6);
    GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN6);
}

void Debug_IO_High(void)
{
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN6);
}

void Debug_IO_Low(void)
{
    GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN6);
}

void Debug_IO_Toggle(void)
{
    GPIO_toggleOutputOnPin(GPIO_PORT_P1,GPIO_PIN6);
}
#endif

