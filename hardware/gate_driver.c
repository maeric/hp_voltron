#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "hardware/gate_driver.h"

void GD_Gate_Init(void)
{
    //gate H init
    GATE_H_PORT_DIR |= GATE_H_PIN;
    GATE_H_PORT_OUT &=~ GATE_H_PIN;//init as low level when power on

    //gate L init
    GATE_L_PORT_DIR |= GATE_L_PIN;
    GATE_L_PORT_OUT &=~ GATE_L_PIN;//init as low level when power on
}



void GD_Gate_Ctrl(gate_select_t gate,onoff_state_t state)
{
    if(gate == GATE_H)
    {
        if(state == ON)
        {
            GATE_H_PORT_OUT |= GATE_H_PIN;
        }
        else if(state == OFF)
        {
            GATE_H_PORT_OUT &=~ GATE_H_PIN;
        }
    }

    if(gate == GATE_L)
    {
        if(state == ON)
        {
            GATE_L_PORT_OUT |= GATE_L_PIN;
        }
        else if(state == OFF)
        {
            GATE_L_PORT_OUT &=~ GATE_L_PIN;
        }
    }
}


