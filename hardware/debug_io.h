#ifndef HARDWARE_DEBUG_IO_H_
#define HARDWARE_DEBUG_IO_H_

void Debug_IO_Init(void);
void Debug_IO_High(void);
void Debug_IO_Low(void);
void Debug_IO_Toggle(void);

#endif//HARDWARE_DEBUG_IO_H_

