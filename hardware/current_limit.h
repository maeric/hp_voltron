#ifndef HARDWARE_CURRENT_LIMIT_H_
#define HARDWARE_CURRENT_LIMIT_H_

#include "hardware/adc.h"

#define GAIN            (51)
#define R_SHUNT_TOTAL   (0.001)

#define AMPS_TO_ADC(x)  ((uint16_t)((double)(x) * R_SHUNT_TOTAL * GAIN * ADC_RESULT_MAX / ADC_REF_VOLTAGE))

#endif//HARDWARE_UART_H_

