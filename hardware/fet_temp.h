#ifndef HARDWARE_FET_TEMP_H_
#define HARDWARE_FET_TEMP_H_

#include "software_modules/typedef.h"

#define BOM_R21 (21000.0)

#define BOM_TH1_70C   (15184.0)
#define BOM_TH1_75C   (12635.0)
#define BOM_TH1_80C   (10566.0)
#define BOM_TH1_88C   (8319.0)
#define BOM_TH1_90C   (7481.0)
#define BOM_TH1_92C   (7312.0)
#define BOM_TH1_100C  (5384.0)
#define BOM_TH1_110C  (3934.0)
#define BOM_TH1_120C  (2916.0)


#define FET_NTC_LIMIT_CALC(x) (((x) / ((x) + BOM_R21)) * ADC_RESULT_MAX)

#endif//HARDWARE_FET_TEMP_H_

