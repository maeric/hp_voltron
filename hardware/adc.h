#ifndef HARDWARE_ADC_H_
#define HARDWARE_ADC_H_

#define ADC_CH_CURRENT  ADCINCH_1

#define ADC_CH_VBUS     ADCINCH_4
#define ADC_VBUS_PORT   GPIO_PORT_P1
#define ADC_VBUS_PIN    GPIO_PIN4

#define ADC_CH_FET_TEMP ADCINCH_5



#define ADC_RESULT_MAX  (1023U)
#define ADC_REF_VOLTAGE (3.3)

void ADC_Init(void);
uint16_t ADC_Read_Value(uint16_t adc_channel);

#endif//HARDWARE_ADC_H_

