#ifndef HARDWARE_UART_H_
#define HARDWARE_UART_H_

void UART_Init(void);
void UART_Send_String(char *str);
void UART_Send_Unit16(uint16_t data_to_send);

#endif//HARDWARE_UART_H_

