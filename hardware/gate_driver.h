#ifndef HARDWARE_GATE_DRIVER_H_
#define HARDWARE_GATE_DRIVER_H_

#include "software_modules/typedef.h"

typedef enum
{
    GATE_H,
    GATE_L,
    NUM_GATE_SELECT,
}gate_select_t;


#define GATE_H_PORT_DIR P2DIR
#define GATE_H_PORT_OUT P2OUT
#define GATE_H_PIN      BIT(0)      

#define GATE_L_PORT_DIR P2DIR
#define GATE_L_PORT_OUT P2OUT
#define GATE_L_PIN      BIT(7)

void GD_Gate_Init(void);
void GD_Gate_Ctrl(gate_select_t gate,onoff_state_t state);

#endif//HARDWARE_GATE_DRIVER_H_

