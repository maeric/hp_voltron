#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "hardware/uart.h"

#if CFG_UART_ENABLE == true

static volatile char string_buf[5];

static void Unit16_To_String(char *str, uint16_t num);


void UART_Init(void)
{
    //Configure UART pins
    GPIO_setAsPeripheralModuleFunctionInputPin(
        GPIO_PORT_P1,
        GPIO_PIN7,
        GPIO_PRIMARY_MODULE_FUNCTION
    );

    //Configure UART
    //SMCLK = 1MHz, Baudrate = 115200
    //UCBRx = 8, UCBRFx = 0, UCBRSx = 0xD6, UCOS16 = 0
    EUSCI_A_UART_initParam param = {0};
    param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    param.clockPrescalar = 69;//112;//96;//8
    param.firstModReg = 0;
    param.secondModReg = 0;//0xD6;
    param.parity = EUSCI_A_UART_NO_PARITY;
    param.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    param.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    param.uartMode = EUSCI_A_UART_MODE;
    param.overSampling = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;

    if (STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A0_BASE, &param)) {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A0_BASE);

    //EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE,EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable USCI_A0 RX interrupt
    //EUSCI_A_UART_enableInterrupt(EUSCI_A0_BASE,EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable global interrupts
    //__enable_interrupt();
}

void UART_Send_String(char *str)
{
    while(*str!='\0')
    {
        EUSCI_A_UART_transmitData(EUSCI_A0_BASE, *str);
        str++;
    }
}

static void Unit16_To_String(char *str, uint16_t num)
{
    /*
    uint8_t i;

    for(i=0;i<4;i++)
    {
        str[i]=(num % muti_array[i+1]) / muti_array[i]+3;
    }

    str[5]=num/10000;
    */

   str[0]=num / 10000 +'0';
   str[1]=(num % 10000) / 1000 +'0';
   str[2]=(num % 1000) / 100 +'0';
   str[3]=(num % 100) / 10 +'0';
   str[4]=num % 10 +'0';
}

void UART_Send_Unit16(uint16_t data_to_send)
{
    Unit16_To_String(string_buf,data_to_send);
    UART_Send_String(string_buf);
}

#endif


