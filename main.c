#include <msp430.h> 
#include "Driverlib/MSP430FR2xx_4xx/driverlib.h"
#include "stdint.h"
#include "config.h"

#include "hardware/gate_driver.h"
#include "hardware/fault_output.h"
#include "hardware/uart.h"
#include "hardware/debug_io.h"
#include "hardware/adc.h"
#include "hardware/current_limit.h"
#include "hardware/fet_temp.h"
#include "hardware/vbus_divider.h"

#include "software_modules/delay.h"
#include "software_modules/shutdown.h"

void CLK_Init(void);

/**
 * main.c
 */
int main(void)
{
    volatile uint16_t fet_temp_adc;
    volatile uint16_t vbus_threshold_max;
    volatile uint16_t vbus_threshold_min;
    static volatile bool gate_h_state;

    WDT_A_hold(WDT_A_BASE);//Stop Watchdog Time

    Fault_Out_Init();
    CLK_Init();
    GD_Gate_Init();

#if CFG_UART_ENABLE == true
    UART_Init();
#endif

#if CFG_DEBUG_IO_ENABLE == true
    Debug_IO_Init();
#endif

    ADC_Init();
    Delay_Init();

    PMM_unlockLPM5();
    __enable_interrupt();

    Delay_Ms(250);

    //check all short
    if(ADC_Read_Value(ADC_CH_CURRENT) > AMPS_TO_ADC(5.0))//uint:ampere
    {
        Shutdown_Activate(SD_PO_GATE_ALL_SHORT);
    }

    //check if GATE_H is short
    //GATE_H is already OFF
    GD_Gate_Ctrl(GATE_L,ON);
    Delay_Us(500);
    if(ADC_Read_Value(ADC_CH_CURRENT) > AMPS_TO_ADC(5.0))//uint:ampere
    {
        Shutdown_Activate(SD_PO_GATE_H_SHORT);
    }

    //check open
    GD_Gate_Ctrl(GATE_H,ON);
    //L_Gate is already ON
    Delay_Us(100);
    if(ADC_Read_Value(ADC_CH_CURRENT) < AMPS_TO_ADC(2.0))
    {
        Shutdown_Activate(SD_PO_GATE_H_OR_L_OPEN);
    }

    //check GATE_L is short
    GD_Gate_Ctrl(GATE_H,OFF);
    //GATE_L is already ON
    Delay_Us(500);
    //GATE_H is already OFF
    GD_Gate_Ctrl(GATE_L,OFF);
    Delay_Us(100);
    GD_Gate_Ctrl(GATE_H,ON);
    //GATE_L is already OFF
    Delay_Us(500);
    if(ADC_Read_Value(ADC_CH_CURRENT) > AMPS_TO_ADC(5.0))//uint:ampere
    {
        Shutdown_Activate(SD_PO_GATE_L_SHORT);
    }

    //prepare to inter while loop
    GD_Gate_Ctrl(GATE_H,OFF);
    Delay_Us(100);
    gate_h_state = OFF;
    GD_Gate_Ctrl(GATE_L,ON);
    vbus_threshold_max = VBUS_THREHOLD_HOT_MAX;
    vbus_threshold_min = VBUS_THREHOLD_HOT_MIN;

#if CFG_UART_ENABLE == true
    UART_Send_String("enter loop\r\n");
#endif

    while(true)
    {        
        uint16_t fet_temp_adc = ADC_Read_Value(ADC_CH_FET_TEMP);
        
        //set vbus threshold according FET temperature
        if(fet_temp_adc<FET_NTC_LIMIT_CALC(BOM_TH1_110C))
        {
            //temperature>110C
            Shutdown_Activate(SD_FET_NTC_HOT);
        }
        else if(fet_temp_adc<FET_NTC_LIMIT_CALC(BOM_TH1_92C))
        {
            //temperature:92C-110C
            vbus_threshold_max = VBUS_THREHOLD_HOT_MAX;
            vbus_threshold_min = VBUS_THREHOLD_HOT_MIN;
        }
        else if(fet_temp_adc<FET_NTC_LIMIT_CALC(BOM_TH1_88C))
        {
            //temperature:88C-92C
            //use the threshold value same with last time
        }
        else
        {
            //temperature<88C
            vbus_threshold_max = VBUS_THREHOLD_COLD_MAX;
            vbus_threshold_min = VBUS_THREHOLD_COLD_MIN;
        }

        //check if the vbus exceed threshold
        uint16_t vbus_adc = ADC_Read_Value(ADC_CH_VBUS);
        if(vbus_adc > vbus_threshold_max)
        {
            GD_Gate_Ctrl(GATE_H,ON);
            gate_h_state = ON;
        }
        else if(vbus_adc < vbus_threshold_min)
        {
            GD_Gate_Ctrl(GATE_H,OFF);
            gate_h_state = OFF;
        }
        else
        {
            //vbus_threshold_min < vbus_adc < vbus_threshold_max,
            //keep the same state with last time
        }

        Delay_Us(3);
        
        if(gate_h_state == ON)
        {
            if(ADC_Read_Value(ADC_CH_CURRENT) < AMPS_TO_ADC(2.0))
            {
                Shutdown_Activate(SD_RU_GATE_H_OR_L_OPEN);
            }
        }
        else
        {
            if(ADC_Read_Value(ADC_CH_CURRENT) > AMPS_TO_ADC(5.0))//uint:ampere
            {
                Shutdown_Activate(SD_RU_GATE_H_SHORT);
            }
        }
    }
}



void CLK_Init(void)
{
    /*
    //Set ACLK = REFOCLK with clock divider of 1
    CS_initClockSignal(CS_ACLK,CS_REFOCLK_SELECT,CS_CLOCK_DIVIDER_1);
    //Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK,CS_DCOCLKDIV_SELECT,CS_CLOCK_DIVIDER_1);
    //Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK,CS_DCOCLKDIV_SELECT,CS_CLOCK_DIVIDER_1);
    */

    
       // Configure MCLK for 8MHz sourced from DCO.
    __bis_SR_register(SCG0);                // disable FLL
    CSCTL3 |= SELREF__REFOCLK;              // Set REFO as FLL reference source
    CSCTL1 = DCOFTRIMEN_1 | DCOFTRIM0 | DCOFTRIM1 | DCORSEL_3;// DCOFTRIM=3, DCO Range = 8MHz
    CSCTL2 = FLLD_0 + 243;                  // DCODIV = 8MHz
    __delay_cycles(3);
    __bic_SR_register(SCG0);                // enable FLL

    CSCTL4 = SELMS__DCOCLKDIV | SELA__REFOCLK; // set default REFO(~32768Hz) as ACLK source, ACLK = 32768Hz
                                               // default DCODIV as MCLK and SMCLK source
    
}

