################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../archive/Driverlib/MSP430FR2xx_4xx/adc.c \
../archive/Driverlib/MSP430FR2xx_4xx/crc.c \
../archive/Driverlib/MSP430FR2xx_4xx/cs.c \
../archive/Driverlib/MSP430FR2xx_4xx/ecomp.c \
../archive/Driverlib/MSP430FR2xx_4xx/eusci_a_spi.c \
../archive/Driverlib/MSP430FR2xx_4xx/eusci_a_uart.c \
../archive/Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.c \
../archive/Driverlib/MSP430FR2xx_4xx/eusci_b_spi.c \
../archive/Driverlib/MSP430FR2xx_4xx/framctl.c \
../archive/Driverlib/MSP430FR2xx_4xx/gpio.c \
../archive/Driverlib/MSP430FR2xx_4xx/icc.c \
../archive/Driverlib/MSP430FR2xx_4xx/lcd_e.c \
../archive/Driverlib/MSP430FR2xx_4xx/mpy32.c \
../archive/Driverlib/MSP430FR2xx_4xx/pmm.c \
../archive/Driverlib/MSP430FR2xx_4xx/rtc.c \
../archive/Driverlib/MSP430FR2xx_4xx/sac.c \
../archive/Driverlib/MSP430FR2xx_4xx/sfr.c \
../archive/Driverlib/MSP430FR2xx_4xx/sysctl.c \
../archive/Driverlib/MSP430FR2xx_4xx/tia.c \
../archive/Driverlib/MSP430FR2xx_4xx/timer_a.c \
../archive/Driverlib/MSP430FR2xx_4xx/timer_b.c \
../archive/Driverlib/MSP430FR2xx_4xx/tlv.c \
../archive/Driverlib/MSP430FR2xx_4xx/wdt_a.c 

C_DEPS += \
./archive/Driverlib/MSP430FR2xx_4xx/adc.d \
./archive/Driverlib/MSP430FR2xx_4xx/crc.d \
./archive/Driverlib/MSP430FR2xx_4xx/cs.d \
./archive/Driverlib/MSP430FR2xx_4xx/ecomp.d \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_a_spi.d \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_a_uart.d \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.d \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_b_spi.d \
./archive/Driverlib/MSP430FR2xx_4xx/framctl.d \
./archive/Driverlib/MSP430FR2xx_4xx/gpio.d \
./archive/Driverlib/MSP430FR2xx_4xx/icc.d \
./archive/Driverlib/MSP430FR2xx_4xx/lcd_e.d \
./archive/Driverlib/MSP430FR2xx_4xx/mpy32.d \
./archive/Driverlib/MSP430FR2xx_4xx/pmm.d \
./archive/Driverlib/MSP430FR2xx_4xx/rtc.d \
./archive/Driverlib/MSP430FR2xx_4xx/sac.d \
./archive/Driverlib/MSP430FR2xx_4xx/sfr.d \
./archive/Driverlib/MSP430FR2xx_4xx/sysctl.d \
./archive/Driverlib/MSP430FR2xx_4xx/tia.d \
./archive/Driverlib/MSP430FR2xx_4xx/timer_a.d \
./archive/Driverlib/MSP430FR2xx_4xx/timer_b.d \
./archive/Driverlib/MSP430FR2xx_4xx/tlv.d \
./archive/Driverlib/MSP430FR2xx_4xx/wdt_a.d 

OBJS += \
./archive/Driverlib/MSP430FR2xx_4xx/adc.obj \
./archive/Driverlib/MSP430FR2xx_4xx/crc.obj \
./archive/Driverlib/MSP430FR2xx_4xx/cs.obj \
./archive/Driverlib/MSP430FR2xx_4xx/ecomp.obj \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_a_spi.obj \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_a_uart.obj \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.obj \
./archive/Driverlib/MSP430FR2xx_4xx/eusci_b_spi.obj \
./archive/Driverlib/MSP430FR2xx_4xx/framctl.obj \
./archive/Driverlib/MSP430FR2xx_4xx/gpio.obj \
./archive/Driverlib/MSP430FR2xx_4xx/icc.obj \
./archive/Driverlib/MSP430FR2xx_4xx/lcd_e.obj \
./archive/Driverlib/MSP430FR2xx_4xx/mpy32.obj \
./archive/Driverlib/MSP430FR2xx_4xx/pmm.obj \
./archive/Driverlib/MSP430FR2xx_4xx/rtc.obj \
./archive/Driverlib/MSP430FR2xx_4xx/sac.obj \
./archive/Driverlib/MSP430FR2xx_4xx/sfr.obj \
./archive/Driverlib/MSP430FR2xx_4xx/sysctl.obj \
./archive/Driverlib/MSP430FR2xx_4xx/tia.obj \
./archive/Driverlib/MSP430FR2xx_4xx/timer_a.obj \
./archive/Driverlib/MSP430FR2xx_4xx/timer_b.obj \
./archive/Driverlib/MSP430FR2xx_4xx/tlv.obj \
./archive/Driverlib/MSP430FR2xx_4xx/wdt_a.obj 

OBJS__QUOTED += \
"archive\Driverlib\MSP430FR2xx_4xx\adc.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\crc.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\cs.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\ecomp.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_a_spi.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_a_uart.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_b_i2c.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_b_spi.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\framctl.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\gpio.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\icc.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\lcd_e.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\mpy32.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\pmm.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\rtc.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\sac.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\sfr.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\sysctl.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\tia.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\timer_a.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\timer_b.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\tlv.obj" \
"archive\Driverlib\MSP430FR2xx_4xx\wdt_a.obj" 

C_DEPS__QUOTED += \
"archive\Driverlib\MSP430FR2xx_4xx\adc.d" \
"archive\Driverlib\MSP430FR2xx_4xx\crc.d" \
"archive\Driverlib\MSP430FR2xx_4xx\cs.d" \
"archive\Driverlib\MSP430FR2xx_4xx\ecomp.d" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_a_spi.d" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_a_uart.d" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_b_i2c.d" \
"archive\Driverlib\MSP430FR2xx_4xx\eusci_b_spi.d" \
"archive\Driverlib\MSP430FR2xx_4xx\framctl.d" \
"archive\Driverlib\MSP430FR2xx_4xx\gpio.d" \
"archive\Driverlib\MSP430FR2xx_4xx\icc.d" \
"archive\Driverlib\MSP430FR2xx_4xx\lcd_e.d" \
"archive\Driverlib\MSP430FR2xx_4xx\mpy32.d" \
"archive\Driverlib\MSP430FR2xx_4xx\pmm.d" \
"archive\Driverlib\MSP430FR2xx_4xx\rtc.d" \
"archive\Driverlib\MSP430FR2xx_4xx\sac.d" \
"archive\Driverlib\MSP430FR2xx_4xx\sfr.d" \
"archive\Driverlib\MSP430FR2xx_4xx\sysctl.d" \
"archive\Driverlib\MSP430FR2xx_4xx\tia.d" \
"archive\Driverlib\MSP430FR2xx_4xx\timer_a.d" \
"archive\Driverlib\MSP430FR2xx_4xx\timer_b.d" \
"archive\Driverlib\MSP430FR2xx_4xx\tlv.d" \
"archive\Driverlib\MSP430FR2xx_4xx\wdt_a.d" 

C_SRCS__QUOTED += \
"../archive/Driverlib/MSP430FR2xx_4xx/adc.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/crc.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/cs.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/ecomp.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/eusci_a_spi.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/eusci_a_uart.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/eusci_b_spi.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/framctl.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/gpio.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/icc.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/lcd_e.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/mpy32.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/pmm.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/rtc.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/sac.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/sfr.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/sysctl.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/tia.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/timer_a.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/timer_b.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/tlv.c" \
"../archive/Driverlib/MSP430FR2xx_4xx/wdt_a.c" 


