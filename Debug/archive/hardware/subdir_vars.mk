################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../archive/hardware/adc.c \
../archive/hardware/current_limit.c \
../archive/hardware/fault_output.c \
../archive/hardware/fet_temp.c \
../archive/hardware/gate_driver.c \
../archive/hardware/uart.c \
../archive/hardware/vbus_divider.c 

C_DEPS += \
./archive/hardware/adc.d \
./archive/hardware/current_limit.d \
./archive/hardware/fault_output.d \
./archive/hardware/fet_temp.d \
./archive/hardware/gate_driver.d \
./archive/hardware/uart.d \
./archive/hardware/vbus_divider.d 

OBJS += \
./archive/hardware/adc.obj \
./archive/hardware/current_limit.obj \
./archive/hardware/fault_output.obj \
./archive/hardware/fet_temp.obj \
./archive/hardware/gate_driver.obj \
./archive/hardware/uart.obj \
./archive/hardware/vbus_divider.obj 

OBJS__QUOTED += \
"archive\hardware\adc.obj" \
"archive\hardware\current_limit.obj" \
"archive\hardware\fault_output.obj" \
"archive\hardware\fet_temp.obj" \
"archive\hardware\gate_driver.obj" \
"archive\hardware\uart.obj" \
"archive\hardware\vbus_divider.obj" 

C_DEPS__QUOTED += \
"archive\hardware\adc.d" \
"archive\hardware\current_limit.d" \
"archive\hardware\fault_output.d" \
"archive\hardware\fet_temp.d" \
"archive\hardware\gate_driver.d" \
"archive\hardware\uart.d" \
"archive\hardware\vbus_divider.d" 

C_SRCS__QUOTED += \
"../archive/hardware/adc.c" \
"../archive/hardware/current_limit.c" \
"../archive/hardware/fault_output.c" \
"../archive/hardware/fet_temp.c" \
"../archive/hardware/gate_driver.c" \
"../archive/hardware/uart.c" \
"../archive/hardware/vbus_divider.c" 


