################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../hardware/adc.c \
../hardware/current_limit.c \
../hardware/debug_io.c \
../hardware/fault_output.c \
../hardware/fet_temp.c \
../hardware/gate_driver.c \
../hardware/uart.c \
../hardware/vbus_divider.c 

C_DEPS += \
./hardware/adc.d \
./hardware/current_limit.d \
./hardware/debug_io.d \
./hardware/fault_output.d \
./hardware/fet_temp.d \
./hardware/gate_driver.d \
./hardware/uart.d \
./hardware/vbus_divider.d 

OBJS += \
./hardware/adc.obj \
./hardware/current_limit.obj \
./hardware/debug_io.obj \
./hardware/fault_output.obj \
./hardware/fet_temp.obj \
./hardware/gate_driver.obj \
./hardware/uart.obj \
./hardware/vbus_divider.obj 

OBJS__QUOTED += \
"hardware\adc.obj" \
"hardware\current_limit.obj" \
"hardware\debug_io.obj" \
"hardware\fault_output.obj" \
"hardware\fet_temp.obj" \
"hardware\gate_driver.obj" \
"hardware\uart.obj" \
"hardware\vbus_divider.obj" 

C_DEPS__QUOTED += \
"hardware\adc.d" \
"hardware\current_limit.d" \
"hardware\debug_io.d" \
"hardware\fault_output.d" \
"hardware\fet_temp.d" \
"hardware\gate_driver.d" \
"hardware\uart.d" \
"hardware\vbus_divider.d" 

C_SRCS__QUOTED += \
"../hardware/adc.c" \
"../hardware/current_limit.c" \
"../hardware/debug_io.c" \
"../hardware/fault_output.c" \
"../hardware/fet_temp.c" \
"../hardware/gate_driver.c" \
"../hardware/uart.c" \
"../hardware/vbus_divider.c" 


