################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../software_modules/delay.c \
../software_modules/shutdown.c 

C_DEPS += \
./software_modules/delay.d \
./software_modules/shutdown.d 

OBJS += \
./software_modules/delay.obj \
./software_modules/shutdown.obj 

OBJS__QUOTED += \
"software_modules\delay.obj" \
"software_modules\shutdown.obj" 

C_DEPS__QUOTED += \
"software_modules\delay.d" \
"software_modules\shutdown.d" 

C_SRCS__QUOTED += \
"../software_modules/delay.c" \
"../software_modules/shutdown.c" 


