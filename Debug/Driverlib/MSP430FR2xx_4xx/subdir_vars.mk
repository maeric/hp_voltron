################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Driverlib/MSP430FR2xx_4xx/adc.c \
../Driverlib/MSP430FR2xx_4xx/crc.c \
../Driverlib/MSP430FR2xx_4xx/cs.c \
../Driverlib/MSP430FR2xx_4xx/ecomp.c \
../Driverlib/MSP430FR2xx_4xx/eusci_a_spi.c \
../Driverlib/MSP430FR2xx_4xx/eusci_a_uart.c \
../Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.c \
../Driverlib/MSP430FR2xx_4xx/eusci_b_spi.c \
../Driverlib/MSP430FR2xx_4xx/framctl.c \
../Driverlib/MSP430FR2xx_4xx/gpio.c \
../Driverlib/MSP430FR2xx_4xx/icc.c \
../Driverlib/MSP430FR2xx_4xx/lcd_e.c \
../Driverlib/MSP430FR2xx_4xx/mpy32.c \
../Driverlib/MSP430FR2xx_4xx/pmm.c \
../Driverlib/MSP430FR2xx_4xx/rtc.c \
../Driverlib/MSP430FR2xx_4xx/sac.c \
../Driverlib/MSP430FR2xx_4xx/sfr.c \
../Driverlib/MSP430FR2xx_4xx/sysctl.c \
../Driverlib/MSP430FR2xx_4xx/tia.c \
../Driverlib/MSP430FR2xx_4xx/timer_a.c \
../Driverlib/MSP430FR2xx_4xx/timer_b.c \
../Driverlib/MSP430FR2xx_4xx/tlv.c \
../Driverlib/MSP430FR2xx_4xx/wdt_a.c 

C_DEPS += \
./Driverlib/MSP430FR2xx_4xx/adc.d \
./Driverlib/MSP430FR2xx_4xx/crc.d \
./Driverlib/MSP430FR2xx_4xx/cs.d \
./Driverlib/MSP430FR2xx_4xx/ecomp.d \
./Driverlib/MSP430FR2xx_4xx/eusci_a_spi.d \
./Driverlib/MSP430FR2xx_4xx/eusci_a_uart.d \
./Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.d \
./Driverlib/MSP430FR2xx_4xx/eusci_b_spi.d \
./Driverlib/MSP430FR2xx_4xx/framctl.d \
./Driverlib/MSP430FR2xx_4xx/gpio.d \
./Driverlib/MSP430FR2xx_4xx/icc.d \
./Driverlib/MSP430FR2xx_4xx/lcd_e.d \
./Driverlib/MSP430FR2xx_4xx/mpy32.d \
./Driverlib/MSP430FR2xx_4xx/pmm.d \
./Driverlib/MSP430FR2xx_4xx/rtc.d \
./Driverlib/MSP430FR2xx_4xx/sac.d \
./Driverlib/MSP430FR2xx_4xx/sfr.d \
./Driverlib/MSP430FR2xx_4xx/sysctl.d \
./Driverlib/MSP430FR2xx_4xx/tia.d \
./Driverlib/MSP430FR2xx_4xx/timer_a.d \
./Driverlib/MSP430FR2xx_4xx/timer_b.d \
./Driverlib/MSP430FR2xx_4xx/tlv.d \
./Driverlib/MSP430FR2xx_4xx/wdt_a.d 

OBJS += \
./Driverlib/MSP430FR2xx_4xx/adc.obj \
./Driverlib/MSP430FR2xx_4xx/crc.obj \
./Driverlib/MSP430FR2xx_4xx/cs.obj \
./Driverlib/MSP430FR2xx_4xx/ecomp.obj \
./Driverlib/MSP430FR2xx_4xx/eusci_a_spi.obj \
./Driverlib/MSP430FR2xx_4xx/eusci_a_uart.obj \
./Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.obj \
./Driverlib/MSP430FR2xx_4xx/eusci_b_spi.obj \
./Driverlib/MSP430FR2xx_4xx/framctl.obj \
./Driverlib/MSP430FR2xx_4xx/gpio.obj \
./Driverlib/MSP430FR2xx_4xx/icc.obj \
./Driverlib/MSP430FR2xx_4xx/lcd_e.obj \
./Driverlib/MSP430FR2xx_4xx/mpy32.obj \
./Driverlib/MSP430FR2xx_4xx/pmm.obj \
./Driverlib/MSP430FR2xx_4xx/rtc.obj \
./Driverlib/MSP430FR2xx_4xx/sac.obj \
./Driverlib/MSP430FR2xx_4xx/sfr.obj \
./Driverlib/MSP430FR2xx_4xx/sysctl.obj \
./Driverlib/MSP430FR2xx_4xx/tia.obj \
./Driverlib/MSP430FR2xx_4xx/timer_a.obj \
./Driverlib/MSP430FR2xx_4xx/timer_b.obj \
./Driverlib/MSP430FR2xx_4xx/tlv.obj \
./Driverlib/MSP430FR2xx_4xx/wdt_a.obj 

OBJS__QUOTED += \
"Driverlib\MSP430FR2xx_4xx\adc.obj" \
"Driverlib\MSP430FR2xx_4xx\crc.obj" \
"Driverlib\MSP430FR2xx_4xx\cs.obj" \
"Driverlib\MSP430FR2xx_4xx\ecomp.obj" \
"Driverlib\MSP430FR2xx_4xx\eusci_a_spi.obj" \
"Driverlib\MSP430FR2xx_4xx\eusci_a_uart.obj" \
"Driverlib\MSP430FR2xx_4xx\eusci_b_i2c.obj" \
"Driverlib\MSP430FR2xx_4xx\eusci_b_spi.obj" \
"Driverlib\MSP430FR2xx_4xx\framctl.obj" \
"Driverlib\MSP430FR2xx_4xx\gpio.obj" \
"Driverlib\MSP430FR2xx_4xx\icc.obj" \
"Driverlib\MSP430FR2xx_4xx\lcd_e.obj" \
"Driverlib\MSP430FR2xx_4xx\mpy32.obj" \
"Driverlib\MSP430FR2xx_4xx\pmm.obj" \
"Driverlib\MSP430FR2xx_4xx\rtc.obj" \
"Driverlib\MSP430FR2xx_4xx\sac.obj" \
"Driverlib\MSP430FR2xx_4xx\sfr.obj" \
"Driverlib\MSP430FR2xx_4xx\sysctl.obj" \
"Driverlib\MSP430FR2xx_4xx\tia.obj" \
"Driverlib\MSP430FR2xx_4xx\timer_a.obj" \
"Driverlib\MSP430FR2xx_4xx\timer_b.obj" \
"Driverlib\MSP430FR2xx_4xx\tlv.obj" \
"Driverlib\MSP430FR2xx_4xx\wdt_a.obj" 

C_DEPS__QUOTED += \
"Driverlib\MSP430FR2xx_4xx\adc.d" \
"Driverlib\MSP430FR2xx_4xx\crc.d" \
"Driverlib\MSP430FR2xx_4xx\cs.d" \
"Driverlib\MSP430FR2xx_4xx\ecomp.d" \
"Driverlib\MSP430FR2xx_4xx\eusci_a_spi.d" \
"Driverlib\MSP430FR2xx_4xx\eusci_a_uart.d" \
"Driverlib\MSP430FR2xx_4xx\eusci_b_i2c.d" \
"Driverlib\MSP430FR2xx_4xx\eusci_b_spi.d" \
"Driverlib\MSP430FR2xx_4xx\framctl.d" \
"Driverlib\MSP430FR2xx_4xx\gpio.d" \
"Driverlib\MSP430FR2xx_4xx\icc.d" \
"Driverlib\MSP430FR2xx_4xx\lcd_e.d" \
"Driverlib\MSP430FR2xx_4xx\mpy32.d" \
"Driverlib\MSP430FR2xx_4xx\pmm.d" \
"Driverlib\MSP430FR2xx_4xx\rtc.d" \
"Driverlib\MSP430FR2xx_4xx\sac.d" \
"Driverlib\MSP430FR2xx_4xx\sfr.d" \
"Driverlib\MSP430FR2xx_4xx\sysctl.d" \
"Driverlib\MSP430FR2xx_4xx\tia.d" \
"Driverlib\MSP430FR2xx_4xx\timer_a.d" \
"Driverlib\MSP430FR2xx_4xx\timer_b.d" \
"Driverlib\MSP430FR2xx_4xx\tlv.d" \
"Driverlib\MSP430FR2xx_4xx\wdt_a.d" 

C_SRCS__QUOTED += \
"../Driverlib/MSP430FR2xx_4xx/adc.c" \
"../Driverlib/MSP430FR2xx_4xx/crc.c" \
"../Driverlib/MSP430FR2xx_4xx/cs.c" \
"../Driverlib/MSP430FR2xx_4xx/ecomp.c" \
"../Driverlib/MSP430FR2xx_4xx/eusci_a_spi.c" \
"../Driverlib/MSP430FR2xx_4xx/eusci_a_uart.c" \
"../Driverlib/MSP430FR2xx_4xx/eusci_b_i2c.c" \
"../Driverlib/MSP430FR2xx_4xx/eusci_b_spi.c" \
"../Driverlib/MSP430FR2xx_4xx/framctl.c" \
"../Driverlib/MSP430FR2xx_4xx/gpio.c" \
"../Driverlib/MSP430FR2xx_4xx/icc.c" \
"../Driverlib/MSP430FR2xx_4xx/lcd_e.c" \
"../Driverlib/MSP430FR2xx_4xx/mpy32.c" \
"../Driverlib/MSP430FR2xx_4xx/pmm.c" \
"../Driverlib/MSP430FR2xx_4xx/rtc.c" \
"../Driverlib/MSP430FR2xx_4xx/sac.c" \
"../Driverlib/MSP430FR2xx_4xx/sfr.c" \
"../Driverlib/MSP430FR2xx_4xx/sysctl.c" \
"../Driverlib/MSP430FR2xx_4xx/tia.c" \
"../Driverlib/MSP430FR2xx_4xx/timer_a.c" \
"../Driverlib/MSP430FR2xx_4xx/timer_b.c" \
"../Driverlib/MSP430FR2xx_4xx/tlv.c" \
"../Driverlib/MSP430FR2xx_4xx/wdt_a.c" 


